# gitlab-runner

This project is the instructions on how to install gitlab k8s executor.

## 目錄
1. [安裝 GitLab K8s executor](1-install-gitLab-k8s-executor.md)
2. [使用 GitLab CI/CD 部署應用程式](2-deploy-app-by-gitlab-ci.md)
3. [配置 GitLab K8s executor 的角色權限](3-binding-cluster-admin-to-runner.md)
4. [調整 GitLab K8s executor 的角色權限](4-configure-rbac-based-on-polp.md)