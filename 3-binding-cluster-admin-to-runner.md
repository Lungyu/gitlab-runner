## 執行步驟
依據[網路解答](https://gitlab.com/gitlab-org/gitlab/-/issues/25135#note_215536652)修正錯誤
### 配置 RBAC
1. 編輯 `files/rbac-cluster-admin.yaml` 以修正 namespace
    ```sh
    # Linux:
    $ sed -i 's/<playerid>/'"$USER"'/g' rbac-cluster-admin.yaml

    # Mac:
    $ sed -i "" 's/<playerid>/'"$USER"'/g' rbac-cluster-admin.yaml
    ```
2. 建立 ClusterRole 及 ClusterRoleBinding
    ```sh
    $ kubectl apply -f rbac-cluster-admin.yaml
    clusterrole.rbac.authorization.k8s.io/gitlab-runner-admin created
    clusterrolebinding.rbac.authorization.k8s.io/gitlab-runner created
    ```
3. 重新執行 GitLab CI/CD。在完成配置 RBAC 後，結果為：
   ![](images/pass-with-rbac.png)

4. 設定 GitLab 專案的環境變數 `NAMESPACE`

請於自己的 app-nginx 的 Settings > CI/CD > Variables > 設定環境變數 `NAMESPACE` 與設定值 `gitlab-runner-$USER` (由於我們的環境是多人共用，為避免後續資源衝突，因此資源皆加上識別號)

5. 查看 Runner 可執行的動作
    ```sh
    $ kubectl api-resources -o wide
    ```
    > :exclamation: 可以發現該 GitLab Runner 可存取的資源範圍極廣！

    <details>
    <summary>api-resources</summary>
    <p>

    ```sh
    NAME                              SHORTNAMES   APIVERSION                             NAMESPACED   KIND                             VERBS
    bindings                                       v1                                     true         Binding                          [create]
    componentstatuses                 cs           v1                                     false        ComponentStatus                  [get list]
    configmaps                        cm           v1                                     true         ConfigMap                        [create delete deletecollection get list patch update watch]
    endpoints                         ep           v1                                     true         Endpoints                        [create delete deletecollection get list patch update watch]
    events                            ev           v1                                     true         Event                            [create delete deletecollection get list patch update watch]
    limitranges                       limits       v1                                     true         LimitRange                       [create delete deletecollection get list patch update watch]
    namespaces                        ns           v1                                     false        Namespace                        [create delete get list patch update watch]
    nodes                             no           v1                                     false        Node                             [create delete deletecollection get list patch update watch]
    persistentvolumeclaims            pvc          v1                                     true         PersistentVolumeClaim            [create delete deletecollection get list patch update watch]
    persistentvolumes                 pv           v1                                     false        PersistentVolume                 [create delete deletecollection get list patch update watch]
    pods                              po           v1                                     true         Pod                              [create delete deletecollection get list patch update watch]
    podtemplates                                   v1                                     true         PodTemplate                      [create delete deletecollection get list patch update watch]
    replicationcontrollers            rc           v1                                     true         ReplicationController            [create delete deletecollection get list patch update watch]
    resourcequotas                    quota        v1                                     true         ResourceQuota                    [create delete deletecollection get list patch update watch]
    secrets                                        v1                                     true         Secret                           [create delete deletecollection get list patch update watch]
    serviceaccounts                   sa           v1                                     true         ServiceAccount                   [create delete deletecollection get list patch update watch]
    services                          svc          v1                                     true         Service                          [create delete get list patch update watch]
    mutatingwebhookconfigurations                  admissionregistration.k8s.io/v1        false        MutatingWebhookConfiguration     [create delete deletecollection get list patch update watch]
    validatingwebhookconfigurations                admissionregistration.k8s.io/v1        false        ValidatingWebhookConfiguration   [create delete deletecollection get list patch update watch]
    customresourcedefinitions         crd,crds     apiextensions.k8s.io/v1                false        CustomResourceDefinition         [create delete deletecollection get list patch update watch]
    apiservices                                    apiregistration.k8s.io/v1              false        APIService                       [create delete deletecollection get list patch update watch]
    controllerrevisions                            apps/v1                                true         ControllerRevision               [create delete deletecollection get list patch update watch]
    daemonsets                        ds           apps/v1                                true         DaemonSet                        [create delete deletecollection get list patch update watch]
    deployments                       deploy       apps/v1                                true         Deployment                       [create delete deletecollection get list patch update watch]
    replicasets                       rs           apps/v1                                true         ReplicaSet                       [create delete deletecollection get list patch update watch]
    statefulsets                      sts          apps/v1                                true         StatefulSet                      [create delete deletecollection get list patch update watch]
    tokenreviews                                   authentication.k8s.io/v1               false        TokenReview                      [create]
    localsubjectaccessreviews                      authorization.k8s.io/v1                true         LocalSubjectAccessReview         [create]
    selfsubjectaccessreviews                       authorization.k8s.io/v1                false        SelfSubjectAccessReview          [create]
    selfsubjectrulesreviews                        authorization.k8s.io/v1                false        SelfSubjectRulesReview           [create]
    subjectaccessreviews                           authorization.k8s.io/v1                false        SubjectAccessReview              [create]
    horizontalpodautoscalers          hpa          autoscaling/v1                         true         HorizontalPodAutoscaler          [create delete deletecollection get list patch update watch]
    cronjobs                          cj           batch/v1                               true         CronJob                          [create delete deletecollection get list patch update watch]
    jobs                                           batch/v1                               true         Job                              [create delete deletecollection get list patch update watch]
    certificatesigningrequests        csr          certificates.k8s.io/v1                 false        CertificateSigningRequest        [create delete deletecollection get list patch update watch]
    leases                                         coordination.k8s.io/v1                 true         Lease                            [create delete deletecollection get list patch update watch]
    endpointslices                                 discovery.k8s.io/v1                    true         EndpointSlice                    [create delete deletecollection get list patch update watch]
    events                            ev           events.k8s.io/v1                       true         Event                            [create delete deletecollection get list patch update watch]
    flowschemas                                    flowcontrol.apiserver.k8s.io/v1beta1   false        FlowSchema                       [create delete deletecollection get list patch update watch]
    prioritylevelconfigurations                    flowcontrol.apiserver.k8s.io/v1beta1   false        PriorityLevelConfiguration       [create delete deletecollection get list patch update watch]
    ingressclasses                                 networking.k8s.io/v1                   false        IngressClass                     [create delete deletecollection get list patch update watch]
    ingresses                         ing          networking.k8s.io/v1                   true         Ingress                          [create delete deletecollection get list patch update watch]
    networkpolicies                   netpol       networking.k8s.io/v1                   true         NetworkPolicy                    [create delete deletecollection get list patch update watch]
    runtimeclasses                                 node.k8s.io/v1                         false        RuntimeClass                     [create delete deletecollection get list patch update watch]
    poddisruptionbudgets              pdb          policy/v1                              true         PodDisruptionBudget              [create delete deletecollection get list patch update watch]
    podsecuritypolicies               psp          policy/v1beta1                         false        PodSecurityPolicy                [create delete deletecollection get list patch update watch]
    clusterrolebindings                            rbac.authorization.k8s.io/v1           false        ClusterRoleBinding               [create delete deletecollection get list patch update watch]
    clusterroles                                   rbac.authorization.k8s.io/v1           false        ClusterRole                      [create delete deletecollection get list patch update watch]
    rolebindings                                   rbac.authorization.k8s.io/v1           true         RoleBinding                      [create delete deletecollection get list patch update watch]
    roles                                          rbac.authorization.k8s.io/v1           true         Role                             [create delete deletecollection get list patch update watch]
    priorityclasses                   pc           scheduling.k8s.io/v1                   false        PriorityClass                    [create delete deletecollection get list patch update watch]
    csidrivers                                     storage.k8s.io/v1                      false        CSIDriver                        [create delete deletecollection get list patch update watch]
    csinodes                                       storage.k8s.io/v1                      false        CSINode                          [create delete deletecollection get list patch update watch]
    csistoragecapacities                           storage.k8s.io/v1beta1                 true         CSIStorageCapacity               [create delete deletecollection get list patch update watch]
    storageclasses                    sc           storage.k8s.io/v1                      false        StorageClass                     [create delete deletecollection get list patch update watch]
    volumeattachments                              storage.k8s.io/v1                      false        VolumeAttachment                 [create delete deletecollection get list patch update watch]
    ```

    </p>
    </details>

### 移除 ClusterRoleBinding
基於安全性考量，移除此 ClusterRoleBinding：
```sh
$ kubectl delete clusterrolebinding gitlab-runner -n gitlab-runner-$USER
clusterrolebinding.rbac.authorization.k8s.io "gitlab-runner" deleted
```
